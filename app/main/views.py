from flask import Flask, render_template, session, redirect, url_for, request, flash, abort
from . import main 
import json
from flask import jsonify

@main.route('/', methods = ['GET', 'POST'])
def index():
	return render_template('index.html', title = 'Home')

@main.route('/respons', methods = ['GET', 'POST'])
def process():

	# we need 2 helper mappings, from letters to ints and the inverse
	L2I = dict(zip("ABCDEFGHIJKLMNOPQRSTUVWXYZ",range(26)))
	I2L = dict(zip(range(26),"ABCDEFGHIJKLMNOPQRSTUVWXYZ"))

	user_text  = request.form['text']
	key = int(request.form['key'])

	ciphertext = '';

	if request.form['cipher'] == 'encipher':

		# encipher
		for c in user_text.upper():
			if c.isalpha(): 
				ciphertext += I2L[ (L2I[c] - key)%26 ]
			else: 
				ciphertext += c
		session['cipher'] = ciphertext

	if request.form['cipher'] == 'decipher':

		# decipher
		for c in session.get('cipher').upper():
			if c.isalpha(): 
				ciphertext += I2L[ (L2I[c] + key)%26 ]
			else: 
				ciphertext += c
		session['cipher'] = ciphertext

	response = { 'status': 200, 'user': user_text, 'key': key, 'ciphertext': session.get('cipher')  }



	return jsonify(response)



